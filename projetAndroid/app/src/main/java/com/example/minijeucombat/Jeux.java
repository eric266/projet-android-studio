package com.example.minijeucombat;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.Random;


public class Jeux extends AppCompatActivity {

    ImageButton Pierre, Feuille, Ciseaux, Lezard, Spoke;
    TextView Joueur, Adversiare, Resultat;
    int coupJoueur=-1;
    Random rand=new Random();
    int coupAdversaire=rand.nextInt(5);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jeux);

        Pierre= findViewById(R.id.pierre);
        Feuille= findViewById(R.id.feuille);
        Ciseaux= findViewById(R.id.ciseaux);
        Lezard= findViewById(R.id.lezard);
        Spoke= findViewById(R.id.spoke);

        Joueur=findViewById(R.id.textView4);
        Adversiare=findViewById(R.id.textView5);
        Resultat=findViewById(R.id.textView3);

    }

    public void ValeurCoup(View v){
        if(v.getId()==Pierre.getId()){
            coupJoueur=0;
            Joueur.setText("Pierre");
        }
        else if(v.getId()==Feuille.getId()){
            int coupJoueur=1;
            Joueur.setText("Feuille");
        }
        else if(v.getId()==Ciseaux.getId()){
            int coupJoueur=2;
            Joueur.setText("Ciseaux");
        }
        else if(v.getId()==Lezard.getId()){
            int coupJoueur=3;
            Joueur.setText("Lezard");
        }
        else if(v.getId()==Spoke.getId()){
            int coupJoueur=4;
            Joueur.setText("Spoke");
        }
        ValeurCoupAdversiare(v);

    }

    public void ValeurCoupAdversiare(View v){
        if(coupAdversaire==0){Adversiare.setText("Pierre");}
        if(coupAdversaire==1){Adversiare.setText("Feuille");}
        if(coupAdversaire==2){Adversiare.setText("Ciseaux");}
        if(coupAdversaire==3){Adversiare.setText("Lezard");}
        if(coupAdversaire==4){Adversiare.setText("Spoke");}
        coupAdversaire=rand.nextInt(5);
        ResultatCoup(v);
    }

    public void ResultatCoup(View v){
        if((coupJoueur==0 && (coupAdversaire==1 || coupAdversaire==4)) || (coupJoueur==1&&(coupAdversaire==2||coupAdversaire==3)) || (coupJoueur==2&&(coupAdversaire==0||coupAdversaire==4)) || (coupJoueur==3&&(coupAdversaire==1||coupAdversaire==2)) || (coupJoueur==4&&(coupAdversaire==3||coupAdversaire==1))){
            Resultat.setText("Adversaire gagne");
        }
        else {Resultat.setText("Joueur gagne");}
    }
}
