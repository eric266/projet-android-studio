package com.example.minijeucombat;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;

public class Inscription extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inscription);
    }

    public void Go_Remerciement(View v) {
        Intent intent=new Intent(this,Remerciement.class);
        startActivity(intent);
    }

    public void fermer(View v) {

        finish() ;
    }
}
